<?php

namespace VmdCms\Modules\Subscriptions\Sections;

use VmdCms\CoreCms\Contracts\Dashboard\Forms\FormInterface;
use VmdCms\CoreCms\CoreModules\CoreTranslates\Services\CoreLang;
use VmdCms\CoreCms\Facades\ColumnEditable;
use VmdCms\CoreCms\Facades\Display;
use VmdCms\CoreCms\Facades\Form;
use VmdCms\CoreCms\Facades\FormComponent;
use VmdCms\CoreCms\Models\CmsSection;

class SubscriptionOption extends CmsSection
{
    /**
     * @var string
     */
    protected $slug = 'subscription_options';

    /**
     * @inheritDoc
     */
    public function getTitle() : string
    {
        return 'Subscription Options';
    }

    public function display()
    {
        return Display::dataTable([
            ColumnEditable::text('info.title',CoreLang::get('title'))
                ->setSearchableCallback(function ($query, $search) {
                    $query->orWhereHas('info', function ($q) use ($search) {
                        $q->where('title', 'like', '%' . $search . '%');
                    });
                })
                ->setSortableCallback(function ($query, $sortType) {
                    $query->join('subscription_options_info', function ($join) {
                        $join->on('subscription_options.id', '=', 'subscription_options_info.subscriptions_id');
                    });
                    $query->orderBy('subscription_options_info.title', $sortType)
                        ->selectRaw('subscription_options.*');
                }),
            ColumnEditable::switch('active')->sortable()->alignCenter(),
        ])->orderDefault(function ($query){
            $query->orderBy('order');
        })
            ->setSearchable(true)
            ->orderable();
    }

    /**
     * @return FormInterface
     */
    public function create() : FormInterface
    {
        return $this->edit(null);
    }

    /**
     * @param int|null $id
     * @return FormInterface
     */
    public function edit(?int $id) : FormInterface
    {
        return Form::panel([
            FormComponent::input('info.title',CoreLang::get('title'))->required(),
            FormComponent::radio('active'),
            FormComponent::text('info.description_short',CoreLang::get('description_short')),
            FormComponent::ckeditor('info.description',CoreLang::get('description')),
            FormComponent::seo()
        ]);
    }

    public function getCmsModelClass(): string
    {
        return \VmdCms\Modules\Subscriptions\Models\SubscriptionOption::class;
    }
}
