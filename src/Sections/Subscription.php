<?php

namespace VmdCms\Modules\Subscriptions\Sections;

use VmdCms\CoreCms\Contracts\Dashboard\Forms\FormInterface;
use VmdCms\CoreCms\CoreModules\CoreTranslates\Services\CoreLang;
use VmdCms\CoreCms\Facades\ColumnEditable;
use VmdCms\CoreCms\Facades\Display;
use VmdCms\CoreCms\Facades\Form;
use VmdCms\CoreCms\Facades\FormComponent;
use VmdCms\CoreCms\Models\CmsSection;

class Subscription extends CmsSection
{
    /**
     * @var string
     */
    protected $slug = 'subscriptions';

    /**
     * @inheritDoc
     */
    public function getTitle() : string
    {
        return 'Subscriptions';
    }

    public function display()
    {
        return Display::dataTable([
            ColumnEditable::text('info.title',CoreLang::get('title'))
                ->setSearchableCallback(function ($query, $search) {
                    $query->orWhereHas('info', function ($q) use ($search) {
                        $q->where('title', 'like', '%' . $search . '%');
                    });
                })
                ->setSortableCallback(function ($query, $sortType) {
                    $query->join('subscriptions_info', function ($join) {
                        $join->on('subscriptions.id', '=', 'subscriptions_info.subscriptions_id');
                    });
                    $query->orderBy('subscriptions_info.title', $sortType)
                        ->selectRaw('subscriptions.*');
                }),
            ColumnEditable::switch('active')->sortable()->alignCenter(),
        ])->orderDefault(function ($query){
            $query->orderBy('order');
        })
            ->setSearchable(true)
            ->orderable();
    }

    /**
     * @return FormInterface
     */
    public function create() : FormInterface
    {
        return $this->edit(null);
    }

    /**
     * @param int|null $id
     * @return FormInterface
     */
    public function edit(?int $id) : FormInterface
    {
        return Form::panel([
            FormComponent::input('info.title',CoreLang::get('title'))->required(),
            FormComponent::radio('active'),
            FormComponent::text('info.description_short',CoreLang::get('description_short')),
            FormComponent::checkbox('options')->setDisplayField('info.title'),
        ]);
    }

    public function getCmsModelClass(): string
    {
        return \VmdCms\Modules\Subscriptions\Models\Subscription::class;
    }
}
