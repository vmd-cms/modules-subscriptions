<?php

return [
    "subscriptions_module" => [
        "prefix_list" => "subscriptions",
        "prefix_single" => "subscription",
    ]
];
