<?php
namespace VmdCms\Modules\Subscriptions\database\seeds;

use App\Modules\Subscriptions\Sections\Subscription;
use App\Modules\Subscriptions\Sections\SubscriptionOption;
use VmdCms\CoreCms\CoreModules\CoreTranslates\Services\CoreLang;
use VmdCms\CoreCms\Initializers\AbstractModuleSeeder;

class ModuleSeeder extends AbstractModuleSeeder
{
    /**
     * @inheritDoc
     */
    protected function getSectionsAssoc(): array
    {
        return [
            Subscription::class => CoreLang::get('subscriptions'),
            SubscriptionOption::class => CoreLang::get('subscription_options'),
        ];
    }

    /**
     * @inheritDoc
     */
    protected function getNavigationAssoc(): array
    {
        return [
            [
                "slug" => "subscription_group",
                "title" => CoreLang::get('subscriptions'),
                "is_group_title" => true,
                "order" => 3,
                "children" => [
                    [
                        "slug" => "subscriptions",
                        "title" => CoreLang::get('subscriptions'),
                        "section_class" => Subscription::class,
                        "icon" => "icon icon-card",
                    ],
                    [
                        "slug" => "subscription_options",
                        "title" => CoreLang::get('subscription_options'),
                        "section_class" => SubscriptionOption::class,
                        "icon" => "icon icon-catalog",
                    ]
                ]
            ],
        ];
    }

}
