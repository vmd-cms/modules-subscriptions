<?php

use VmdCms\Modules\Subscriptions\Models\SubscriptionOption as model;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSubscriptionOptionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create(model::table(), function (Blueprint $table) {
            $table->increments(model::getPrimaryField());
            $table->string('key',32)->nullable();
            $table->integer('parent_id')->unsigned()->nullable();
            $table->string('image',255)->nullable();
            $table->text('data')->nullable();
            $table->boolean('active')->default(false);
            $table->integer('order')->unsigned()->default(1);
            $table->timestamps();
        });

        Schema::table(model::table(), function (Blueprint $table){
            $table->foreign('parent_id', model::table() . '_parent_id_fk')
                ->references('id')->on(model::table())
                ->onUpdate('CASCADE')->onDelete('SET NULL');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists(model::table());
    }
}
