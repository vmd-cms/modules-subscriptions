<?php

use VmdCms\Modules\Subscriptions\Models\Subscription as FirstModel;
use VmdCms\Modules\Subscriptions\Models\SubscriptionOption as SecondModel;
use VmdCms\Modules\Subscriptions\Models\SubscriptionsOptions as CrossModel;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSubscriptionsOptionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create(CrossModel::table(), function (Blueprint $table) {
            $table->increments('id');
            $table->integer(FirstModel::table() . '_id')->unsigned();
            $table->integer(SecondModel::table() . '_id')->unsigned();
            $table->integer( 'order')->default(0)->unsigned();
            $table->timestamps();
            $table->unique([FirstModel::table() . '_id', SecondModel::table() . '_id'],'subs_opts_sub_id_sub_opt_id_unique');
        });
        Schema::table(CrossModel::table(), function (Blueprint $table){
            $table->foreign(FirstModel::table() . '_id', 'subs_opts_sub_id_fk')
                ->references(FirstModel::getPrimaryField())->on(FirstModel::table())
                ->onUpdate('CASCADE')->onDelete('CASCADE');
            $table->foreign(SecondModel::table() . '_id', 'subs_opts_sub_opt_id_fk')
                ->references(SecondModel::getPrimaryField())->on(SecondModel::table())
                ->onUpdate('CASCADE')->onDelete('CASCADE');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists(CrossModel::table());
    }
}
