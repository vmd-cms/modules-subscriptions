<?php

use VmdCms\CoreCms\CoreModules\Languages\Models\CoreLanguage;
use VmdCms\Modules\Subscriptions\Models\SubscriptionInfo as model;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSubscriptionsInfoTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create(model::table(), function (Blueprint $table){
            $table->increments('id');
            $table->string(model::LANG_KEY,5)->nullable();
            $table->integer(model::getForeignField())->unsigned();
            $table->string('url')->nullable();
            $table->string('title',255)->nullable();
            $table->text('description_short')->nullable();
            $table->longText('description')->nullable();
            $table->timestamps();
            $table->unique([model::LANG_KEY, model::getForeignField()]);
        });

        Schema::table(model::table(), function (Blueprint $table){
            $table->foreign(model::getForeignField(), model::table() . '_' . model::getForeignField().'_fk')
                ->references(model::getBaseModelClass()::getPrimaryField())->on(model::getBaseModelClass()::table())
                ->onUpdate('CASCADE')->onDelete('CASCADE');
        });

        Schema::table(model::table(), function (Blueprint $table){
            $table->foreign(model::LANG_KEY, model::table() . '_' . model::LANG_KEY.'_fk')
                ->references(CoreLanguage::getPrimaryField())->on(CoreLanguage::table())
                ->onUpdate('CASCADE');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists(model::table());
    }
}
