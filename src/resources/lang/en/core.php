<?php

return [
    /**
     * fields
     **/

    /**
     * pages
     */
    'subscriptions' => 'Subscriptions',
    'subscription_options' => 'Options',

    /**
     * messages
     **/

    /**
     * buttons
     **/

    /**
     * validates
     */
];
