<?php

return [
    /**
     * fields
     **/

    /**
     * pages
     */
    'subscriptions' => 'Подписки',
    'subscription_options' => 'Опции',

    /**
     * messages
     **/

    /**
     * buttons
     **/

    /**
     * validates
     */
];
