<?php

namespace VmdCms\Modules\Subscriptions\Initializers;;

use VmdCms\CoreCms\Initializers\AbstractModuleInitializer;

class ModuleInitializer extends AbstractModuleInitializer
{
    const SLUG = 'subscriptions';
    const ALIAS = 'Subscriptions';

    public function __construct()
    {
        parent::__construct();
    }

    /**
     * @inheritDoc
     */
    public static function moduleAlias(): string
    {
        return self::ALIAS;
    }

    /**
     * @inheritDoc
     */
    public static function moduleSlug(): string
    {
        return self::SLUG;
    }

    public function seedCoreEvents()
    {
    }
}
