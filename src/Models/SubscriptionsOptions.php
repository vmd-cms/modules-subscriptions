<?php

namespace VmdCms\Modules\Subscriptions\Models;

use VmdCms\CoreCms\Models\CmsCrossModel;

class SubscriptionsOptions extends CmsCrossModel
{
    public static function table(): string
    {
        return 'subscriptions_options';
    }

    public function getBaseModelClass(): string
    {
        return Subscription::class;
    }

    public function getTargetModelClass(): string
    {
        return SubscriptionOption::class;
    }
}
