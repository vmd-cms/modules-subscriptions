<?php

namespace VmdCms\Modules\Subscriptions\Models;

use VmdCms\CoreCms\Contracts\Models\ActivableInterface;
use VmdCms\CoreCms\Contracts\Models\HasInfoInterface;
use VmdCms\CoreCms\Contracts\Models\OrderableInterface;
use VmdCms\CoreCms\Exceptions\Models\NotCmsCrossModelException;
use VmdCms\CoreCms\Exceptions\Models\NotCmsModelException;
use VmdCms\CoreCms\Models\CmsModel;
use VmdCms\CoreCms\Traits\Models\Activable;
use VmdCms\CoreCms\Traits\Models\HasInfo;
use VmdCms\CoreCms\Traits\Models\Orderable;
use VmdCms\CoreCms\Traits\Related\HasCrossRelatedService;

class Subscription extends CmsModel implements OrderableInterface,ActivableInterface,HasInfoInterface
{
    use Orderable,Activable,HasInfo,HasCrossRelatedService;

    public static function table(): string
    {
        return 'subscriptions';
    }

    /**
     * @param $value
     * @throws NotCmsModelException
     */
    public function setOptionsAttribute($value)
    {
        $this->getCrossRelatedService()
            ->saveCrossRelation('options',$value,SubscriptionsOptions::class);
    }

    /**
     * @return mixed
     * @throws NotCmsCrossModelException
     */
    public function options()
    {
        return  $this->getCrossRelatedService()
            ->setCrossModel(SubscriptionsOptions::class)
            ->setRelatedModel(SubscriptionOption::class)
            ->hasThroughCrossModelWrapper('hasManyThrough');
    }
}
