<?php

namespace VmdCms\Modules\Subscriptions\Models;

use VmdCms\CoreCms\Models\CmsInfoModel;

class SubscriptionInfo extends CmsInfoModel
{
    public static function table(): string
    {
        return 'subscriptions_info';
    }
}
