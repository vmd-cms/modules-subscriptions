<?php

namespace VmdCms\Modules\Subscriptions\Models;

use VmdCms\CoreCms\Models\CmsInfoModel;

class SubscriptionOptionInfo extends CmsInfoModel
{
    public static function table(): string
    {
        return 'subscription_options_info';
    }
}
