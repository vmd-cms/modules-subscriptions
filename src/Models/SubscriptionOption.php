<?php

namespace VmdCms\Modules\Subscriptions\Models;

use VmdCms\CoreCms\Contracts\Models\ActivableInterface;
use VmdCms\CoreCms\Contracts\Models\HasInfoInterface;
use VmdCms\CoreCms\Contracts\Models\OrderableInterface;
use VmdCms\CoreCms\Contracts\Models\TreeableInterface;
use VmdCms\CoreCms\Models\CmsModel;
use VmdCms\CoreCms\Traits\Models\Activable;
use VmdCms\CoreCms\Traits\Models\HasInfo;
use VmdCms\CoreCms\Traits\Models\Orderable;
use VmdCms\CoreCms\Traits\Models\Treeable;

class SubscriptionOption extends CmsModel implements TreeableInterface,OrderableInterface,HasInfoInterface,ActivableInterface
{
    use Treeable,Orderable,HasInfo,Activable;

    public static function table(): string
    {
        return 'subscription_options';
    }
}
